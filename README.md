# dmenu-sorcery
a custom build of suckless's dmenu (dynamic menu for X) <br>
it is suitable with the colors of matcha gtk theme; <br>
https://github.com/vinceliuice/Matcha-gtk-theme
###
## recommended to use it with these custom builds of suckless software: 
dwm-sorcery; https://gitlab.com/sorcery0358/dwm-sorcery <br>
st-sorcery; https://gitlab.com/sorcery0358/st-sorcery <br>
slock-sorcery; https://gitlab.com/sorcery0358/slock-sorcery
## patches
this custom build of dmenu includes these patches:
###
case-insensitive; https://tools.suckless.org/dmenu/patches/case-insensitive <br>
highpriority; https://tools.suckless.org/dmenu/patches/highpriority <br>
line height; https://tools.suckless.org/dmenu/patches/line-height <br>
numbers; https://tools.suckless.org/dmenu/patches/numbers <br>
xyw; https://tools.suckless.org/dmenu/patches/xyw
## installation
	$ cd dmenu-sorcery/dmenu-5.0
	$ sudo make clean install
## customization
to customize edit `dmenu-sorcery/dmenu-5.0/config.def.h` and run these commands;
###
	$ sudo cp config.def.h config.h
	$ sudo make clean install
##
`dmenu-sorcery/dmenu-5.0.tar.gz` is the vanilla version of dmenu
